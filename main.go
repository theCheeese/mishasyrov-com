package main

import (
    "github.com/gin-gonic/gin"
)

func main() {
    r := gin.Default()
    r.LoadHTMLGlob("templates/*")
    r.Static("/assets", "./assets")

    r.GET("/", func(c *gin.Context) {
        c.HTML(200, "index.html", gin.H{
            "page_title": "Index",
	})
    })

    r.GET("/experience", func(c *gin.Context) {
	c.HTML(200, "experience.html", gin.H{
	    "page_title": "Professional Experience",
	})
    })

    r.GET("/projects", func(c *gin.Context) {
        c.HTML(200, "projects.html", gin.H{
	    "page_title": "Portfolio",
	})
    })

    r.GET("/services", func(c *gin.Context) {
        c.HTML(200, "services.html", gin.H{
	    "page_title": "Services",
	})
    })

    r.GET("/contact", func(c *gin.Context) {
        c.HTML(200, "contact.html", gin.H{
	    "page_title": "Contact",
        })
    })

    r.NoRoute(func(c *gin.Context) {
        c.HTML(404, "404.html", gin.H{
	    "page_title": "Not Found",
	})
    })

    r.Run()
}
