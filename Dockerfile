FROM golang:buster

WORKDIR /srv/www
COPY . .
RUN go mod download

EXPOSE 8080

ENTRYPOINT ["go", "run", "main.go"]
